import re
from datetime import datetime
from collections import OrderedDict
from time import strptime
from dateutil import parser as dateparser #python-dateutil
from BeautifulSoup import BeautifulSoup
import pub_type_normalizer
import accent_remover
import data_cleanser
import sys
#from collections import OrderedDict
#from datetime import datetime

_max_authors = 30

month_keys = {
    "jan": 1,
    "january": 1,
    "feb": 2,
    "february": 2,
    "march": 3,
    "mar": 3,
    "apr": 4,
    "april": 4,
    "may": 5,
    "june": 6,
    "jun": 6,
    "july": 7,
    "jul": 7,
    "aug": 8,
    "august": 8,
    "sept": 9,
    "sep": 9,
    "september": 9,
    "october": 10,
    "Oct": 10,
    "nov": 11,
    "november": 11,
    "dec": 12,
    "december": 12 }

class Article:

    def __init__(self, soup_article):
        self.__soup_article = soup_article
        self.__article_dict = OrderedDict()
        self.__pmid = self.__set_pmid()
        self.__set_system_date()
        self.__set_defaults()
        try:
            self.__set_journal()
            self.__set_article_title()
            self.__set_article_type()
            self.__set_publication_status()
            self.__set_article_type()
            self.__set_major_mesh_terms()
            self.__set_abstract()
            self.__set_authors()
            self.__set_dates()
        except Exception, e:
            message = self.__pmid + " - " + str(e)
            print message
            self.__audit(message, 1)
            raise

    def get_article_dict(self):
        return self.__article_dict

    def __set_defaults(self):
        self.__article_dict["system"] = {}
        self.__article_dict["system"]["audit"] = []
        self.__article_dict["system"]["exclude"] = 0
        self.__article_dict["system"]["red_flag"] = 0

    def __audit(self, message, status):
        self.__article_dict["system"]["audit"].append({"message": message, "status": status})
        if status:
            self.__red_flag()

    def __exclude_article(self):
        self.__article_dict["system"]["exclude"] = 1

    def __red_flag(self):
        self.__article_dict["system"]["red_flag"] += 1

    def __get_element_text (self, soup_object, element_id):
        if soup_object:
            return soup_object.find(element_id).text if soup_object.find(element_id) else ""
        return ""

    def __set_system_date(self):
        self.__article_dict["system_date"] = datetime.utcnow()

    def __set_pmid(self):
        if self.__soup_article.find("pmid"):
            self.__article_dict["_id"] = self.__soup_article.find("pmid").text
            return self.__article_dict["_id"]
        else:
            self.__audit("No pmid found", 1)
            self.__red_flag()

    def __set_journal(self):
        self.__article_dict["journal"] = accent_remover.swap_accents(self.__get_element_text(self.__soup_article, "title"))
        if not self.__article_dict["journal"]:
            self.__article_dict["journal"] = accent_remover.swap_accents(self.__get_element_text(self.__soup_article, "isoabbreviation"))
            if not self.__article_dict["journal"]:
                self.__audit("No journal name title or isoabbreviation found", 1)

    def __set_publication_status(self):
        self.__article_dict["publication_status"] = self.__get_element_text(self.__soup_article, "publicationstatus")

    def __set_article_title(self):
        self.__article_dict["title"] = accent_remover.swap_accents(self.__get_element_text(self.__soup_article, "articletitle"))
        if not self.__article_dict["title"]:
            self.__audit("No article title found", 1)

    def __set_article_type(self):
        self.__article_dict["type"] = self.__get_element_text(self.__soup_article, "publicationtype")
        if "retract" in self.__article_dict["type"].lower():
            self.__exclude_article()
            self.__audit("Excluding article; article retracted", 0)
        elif not self.__article_dict["type"]:
            self.__article_dict["type"] = "General Article"

        self.__article_dict["mamba_type"] = pub_type_normalizer.get_mederi_pub_type(self.__article_dict["type"])

    def __set_major_mesh_terms(self):
        mesh_list = []
        mesh_heading_list = self.__soup_article.findAll("meshheading")
        if mesh_heading_list:
            for mesh_section in mesh_heading_list:
                if "majortopicyn=\"y\"" in str(mesh_section).lower():
                    mesh_soup = BeautifulSoup(str(mesh_section))
                    if mesh_soup:
                        for mesh in mesh_soup:
                            if mesh.find("descriptorname"):
                                mesh_list.append(mesh.find("descriptorname").text.strip())

        #self.__article_dict["major_mesh"] =
        self.__article_dict["major_mesh_list"] = mesh_list

    def __set_abstract(self):
        self.__article_dict["abstract"] = data_cleanser.only_ascii(accent_remover.swap_accents(self.__get_element_text(self.__soup_article, "abstracttext"))).replace("\n", " ")
        if self.__article_dict["abstract"]:
            self.__article_dict["abstract"] = data_cleanser.only_ascii(accent_remover.swap_accents(self.__get_element_text(self.__soup_article, "abstract"))).replace("\n", " ")

    def __set_authors(self):

        self.__article_dict.setdefault("authors", [])
        soup_authors = self.__soup_article.findAll(name="author")
        authors = []
        self.__article_dict["author_count"] = len(soup_authors)

        if len(soup_authors) > _max_authors:
            self.__audit(str(len(soup_authors)) + " authors found (exceeds application limit " + str(_max_authors) + ")", 0)
            self.__exclude_article()

        for i_author in range(0, len(soup_authors)):
            soup_author = soup_authors[i_author]

            author = OrderedDict()

            if i_author == 0:
                author ["authorship_index"] = 0
            elif i_author == len(soup_authors) - 1:
                author ["authorship_index"] = -1
            else:
                author["authorship_index"] = i_author

            if soup_author.find("collectivename"):

                author["last_name"] = accent_remover.swap_accents(self.__get_element_text(soup_author, "collectivename"))
                author["first_name"] = ""
                author["is_person"] = 0

            else:

                author["last_name"] = accent_remover.swap_accents(self.__get_element_text(soup_author, "lastname"))
                if soup_author.find("forename"):
                    author["first_name"]  = accent_remover.swap_accents(self.__get_element_text(soup_author, "forename"))
                elif soup_author.find("firstname"):
                    author["first_name"]  = accent_remover.swap_accents(self.__get_element_text(soup_author, "firstname"))
                else:
                    author["first_name"] = ""

                author["suffix"] = accent_remover.swap_accents(self.__get_element_text(soup_author, "suffix"))
                author["is_person"] = 1
            author["affiliation"] = data_cleanser.strip_tags(accent_remover.swap_accents(self.__get_element_text(soup_author, "affiliation"))).encode('ascii',errors='ignore')
            self.__article_dict["authors"].append(author)

        #print self.__article_dict["authors"]

    def __set_dates(self):
        self.__article_dict["publication_date"], good_publication_date = self.__get_pub_date(self.__soup_article.find("articledate"))

        if not self.__article_dict["publication_date"]:
            self.__article_dict["publication_date"], good_publication_date = self.__get_pub_date(self.__soup_article.find("pubdate"))

        if not self.__article_dict["publication_date"]:
            self.__article_dict["publication_date"], good_publication_date = self.__get_medline_date(self.__soup_article.find("medlinedate"))

        self.__article_dict["pubmed_added_date"], good_pubmed_date = self.__get_pub_date(self.__soup_article.find("pubmedpubdate", {"pubstatus": "pubmed"}))

        if good_publication_date == 2:
            self.__article_dict["pub_display_date"] = self.__article_dict["publication_date"]
        else:
            self.__article_dict["pub_display_date"] = self.__article_dict["pubmed_added_date"]

        try:
            self.__article_dict["year"] = self.__article_dict["pub_display_date"].year
        except:
            self.__article_dict["year"] = ""
            self.__audit("Display date year is empty", 1)

    def __get_pub_date(self, soup_pub_date):
        if soup_pub_date:
            pub_date_list = []
            if soup_pub_date.find("year"):
                pub_date_list.append(soup_pub_date.find("year").text)

                try:
                    if soup_pub_date.find("month"):
                        month_text = soup_pub_date.find("month").text
                        if unicode(month_text).isnumeric():
                            pub_date_list.append(month_text)
                        elif len(month_text) >= 3:
                            try:
                                pub_date_list.append(strptime(month_text[0:3],'%b').tm_mon)
                            except:
                                pub_date_list.append(month_text)
                        else:
                            pub_date_list.append(month_text)

                        if soup_pub_date.find("day"):
                            pub_date_list.append(soup_pub_date.find("day").text)
                        else:
                            pub_date_list.append("01")
                        return dateparser.parse(str.join("-", [str(dt) for dt in pub_date_list])), 2
                    else:
                        pub_date_list.append("01")
                        pub_date_list.append("01")
                        return dateparser.parse(str.join("-", [str(dt) for dt in pub_date_list])), 1
                except:
                    pub_date_list.append("01")
                    pub_date_list.append("01")
                    return dateparser.parse(str.join("-", [str(dt) for dt in pub_date_list])), 1

        return None, 0

    def __get_medline_date(self, soup_pub_date):
        try:

            if soup_pub_date:
                if re.match("[0-9][0-9][0-9][0-9]", soup_pub_date.text):
                    pub_date_list = []
                    split = soup_pub_date.text.lower().split(" ")
                    if len(split) > 1:
                        pub_date_list.append(split[0])
                        if "-" in split[1]:
                            split_month = split[1].split("-")
                            if split_month[0] in month_keys:
                                pub_date_list.append(month_keys[split_month[0]])
                                pub_date_list.append("01")
                                return dateparser.parse(str.join("-", [str(dt) for dt in pub_date_list])), 2
                            else:
                                pub_date_list.append("01")
                                pub_date_list.append("01")
                                return dateparser.parse(str.join("-", [str(dt) for dt in pub_date_list])), 1
                        else:
                            pub_date_list.append("01")
                            pub_date_list.append("01")
                            return dateparser.parse(str.join("-", [str(dt) for dt in pub_date_list])), 1
                    else:
                        if "-" in split[0] and len(split[0]) > 4:
                            pub_date_list.append(split[0][0:4])
                        else:
                            pub_date_list.append(split[0])

                        pub_date_list.append("01")
                        pub_date_list.append("01")
                        #print pub_date_list
                        return dateparser.parse(str.join("-", [str(dt) for dt in pub_date_list])), 1
            return None, 0

        except:
            return None, 0

