# -*- coding: UTF-8 -*-
import string
import re
from textblob import TextBlob
from nltk.corpus import stopwords
from HTMLParser import HTMLParser


_stop_words_pattern = re.compile(r'\b(' + r'|'.join(stopwords.words('english')) + r')\b\s*')
_break_up_words_pattern =  re.compile("([\w][\w]*)")

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def only_ascii(s):
    return filter(lambda x: x in string.printable, s)

def remove_noise_words(text):
    return _stop_words_pattern.sub("", text).replace(" ,",",").strip()

def get_words(text):
    words = _break_up_words_pattern.findall(remove_noise_words(text.lower()))
    words = [w.strip() for w in words
             if len(w) > 2 and len(w) < 20
             and w not in ["also", "may", "however", "pubmed", "copyright", "rights", "inc", "the", "however","all", "for", "but", "than", "that"]
             and w[0].isdigit() == False]
    return words

def get_noun_phrases(text):
    return TextBlob(text).noun_phrases

def extract_words(text):
    return get_words(text)

def remove_punctuation(text):
    return re.sub(r"['_,!\-\"\\/}{?\.\*\|\(\)\;]", " ", text).strip()

def remove_extra_spaces(text):
    return re.sub("\s+", " ", text)
