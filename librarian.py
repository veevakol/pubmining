from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

import inspect

_db_name = "pubmed"
_col_article_name = "article"
_col_precursor_name = "precursor"
_col_matrix_name = "matrix"
_col_cluster_name = "cluster"
_col_similitude_name = "similitude"

host = "localhost"


def reset_workspace():
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    collection_names = [_col_matrix_name, _col_precursor_name, _col_cluster_name, _col_similitude_name]
    for col_name in collection_names:
        collection = db[col_name]
        collection.remove()
    client.close()

def save_similitude(similarity):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    similitude_col = db[_col_similitude_name]
    similitude_col.insert(similarity)
    client.close()

def get_authors_with_clusters():
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    cluster_col = db[_col_cluster_name]
    author_ids = list(cluster_col.find({}, {"_id":1}))
    client.close()
    return [id["_id"] for id in author_ids]

def get_author_articles(author_id):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    precursor_col = db[_col_precursor_name]
    articles = list(precursor_col.find({"author_id": author_id}))
    client.close()
    return articles

def get_articles_for_all_authors():
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    precursor_col = db[_col_precursor_name]
    articles = list(precursor_col.find({}))
    client.close()
    return articles

def get_clusters_for_author(author_id):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    cluster_col = db[_col_cluster_name]
    clusters = cluster_col.find_one({"_id": author_id})
    client.close()
    return clusters

def get_clusters_for_author_id_pmid(author_id, pmid):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    precursor_col = db[_col_precursor_name]
    articles = precursor_col.find_one({"_id": pmid + "$" + author_id})
    client.close()
    return articles

def save_clusters(clusters):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    cluster_col = db[_col_cluster_name]
    cluster_col.insert(clusters)
    client.close()

def get_matrix(author_id):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    matrix_col = db[_col_matrix_name]
    matrix = matrix_col.find_one({"_id": author_id})
    client.close()
    return matrix

def save_matrix(matrix):
    #print "-" + inspect.stack()[0][3]
    if matrix:
        client = MongoClient(host)
        db = client[_db_name]
        matrix_col = db[_col_matrix_name]
        matrix_col.insert(matrix)
        client.close()

def get_articles(pmids_list):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    article_col = db[_col_article_name]
    #{$and: [{ "system.exclude": 1},{ "_id": {"$in": ["23169510","25712686"]}}]}, { "_id": 1 }
    articles = list(article_col.find({"$and":[{"system.exclude": 0}, { "_id": {"$in": pmids_list}}]}))
    client.close()
    return articles

def get_articles_not_in_mongodb(pmids_list):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    article_col = db[_col_article_name]
    result = article_col.find({ "_id": {"$in": pmids_list}}, { "_id": 1 })
    found_pmids = [pmid.values()[0] for pmid in result]
    client.close()
    return list(set(pmids_list) - set(found_pmids))

def save_articles(articles_list):
    #print "-" + inspect.stack()[0][3]
    if articles_list:
        client = MongoClient(host)
        db = client[_db_name]
        article_col = db[_col_article_name]
        duplicates = 0
        for article in articles_list:
            try:
                article_col.insert(article)
            except DuplicateKeyError:
                duplicates += 1
        if duplicates > 0:
            print "MongoDB duplicate key error: %d articles already in system" % duplicates

        client.close()

def save_precursors(precursors_list):
    #print "-" + inspect.stack()[0][3]
    if precursors_list:
        client = MongoClient(host)
        db = client[_db_name]
        precursor_col = db[_col_precursor_name]
        precursor_col.insert_many(precursors_list)
        client.close()

def get_precursors(author_id):
    #print "-" + inspect.stack()[0][3]
    client = MongoClient(host)
    db = client[_db_name]
    precursor_col = db[_col_precursor_name]
    precursors = list(precursor_col.find({"author_id": author_id}))
    client.close()
    return precursors

