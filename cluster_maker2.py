import networkx as nx
import mini_nlp
import librarian
import re

matrix = None

def build_graph(graph, similarity):
    for pmid1 in similarity:
        for pmid2 in similarity[pmid1]:
            if not graph.has_edge(pmid1, pmid2):
                graph.add_edge(pmid1, pmid2)

def find_groups(graph):
    sub_graphs = sorted(nx.connected_components(graph))
    clusters = {}
    clusters.setdefault("articles", {})

    for group_id in range(0, len(sub_graphs)):
        for pmid in sub_graphs[group_id]:
            clusters["articles"][pmid] = (group_id + 1)

    clusters["group_count"] = len(sub_graphs)

    return clusters

def check_countries(pmid1, pmid2, countries):
    if pmid1 not in countries or pmid2 not in countries:
        return True
    else:
        result = countries[pmid1] == countries[pmid2]
    return result

def generate_re(x):
    return x.replace(" ", ".*\\b")

def check_name_compatability(pmid1, pmid2):
    global matrix
    a = matrix["article_features"][pmid1]["found_name"]
    b = matrix["article_features"][pmid2]["found_name"]
    split_a = a.split()
    split_b = b.split()
    if a == b:
        return True
    elif len(split_a) != len(split_b):
        return False
    elif not re.match(generate_re(a), b) or not re.match(generate_re(b), a):
        return False
    elif len(split_a) == 2 and len(split_b) == 2 and len(split_a[0]) == len(split_b[0]):
        return True
    elif len(split_a) >= 3 and len(split_b) >= 3 and split_a[1][0] == split_b[1][0]:
        return True
    return False

def calculate_similarity(features, threshold, method, countries, skip_checks=False):
    global matrix
    similarity = {}
    for pmid1 in features:
        similarity.setdefault(pmid1, {})
        for pmid2 in features:

            if pmid1 != pmid2 and features[pmid1] and features[pmid2]:

                if skip_checks or (check_countries(pmid1, pmid2, countries) and check_name_compatability(pmid1, pmid2)):

                    score = 0

                    if method == "text":
                        score = 1 if (features[pmid1] == features[pmid2]) else 0

                    elif method == "fuzzy":
                        score = mini_nlp.calc_cosine_on_text(features[pmid1], features[pmid2])

                    elif method == "list":
                        score = 1 if len(frozenset(features[pmid1]).intersection(features[pmid2])) >= threshold else 0

                    elif method == "dict":
                        if len(features[pmid1]) > 3 and len(features[pmid2]) > 3:
                            if len(frozenset(features[pmid1]).intersection(frozenset(features[pmid2]))):
                                score = mini_nlp.get_cosine_on_vectors(features[pmid1], features[pmid2])

                    if score >= threshold:
                        similarity[pmid1][pmid2] = score

    return similarity

def get_features(matrix):
    affiliations, authors, e_mails, texts, countries, mesh = {}, {}, {}, {}, {}, {}

    for pmid in matrix["article_features"]:

        if "e_mail" in matrix["article_features"][pmid]:
            e_mails[pmid] = matrix["article_features"][pmid]["e_mail"]

        if "affiliation" in matrix["article_features"][pmid]:
            affiliations[pmid] = matrix["article_features"][pmid]["affiliation"]

        if "authors" in matrix["article_features"][pmid]:
            if len(matrix["article_features"][pmid]["authors"]) > 0:
                authors[pmid] = matrix["article_features"][pmid]["authors"]

        if "country" in matrix["article_features"][pmid]:
            if len(matrix["article_features"][pmid]["country"]) > 0:
                countries[pmid] = matrix["article_features"][pmid]["country"]

        if "text" in matrix["article_features"][pmid]:
            if len(matrix["article_features"][pmid]["text"]) > 0:
                texts[pmid] = matrix["article_features"][pmid]["text"]

        if "mesh" in matrix["article_features"][pmid]:
            if len(matrix["article_features"][pmid]["mesh"]) > 0:
                mesh[pmid] = matrix["article_features"][pmid]["mesh"]

    return {
        "affiliations": affiliations,
        "authors": authors,
        "e_mails": e_mails,
        "texts": texts,
        "countries": countries,
        "mesh": mesh
    }

def save_model(similarity, clusters, job):
    librarian.save_similitude ({
        "_id": job["request"]["author_id"],
        "author": job["request"]["first_name"] + " " + job["request"]["last_name"],
        "similarity": similarity
    })
    librarian.save_clusters({
        "_id": job["request"]["author_id"],
        "author": job["request"]["first_name"] + " " + job["request"]["last_name"],
        "cluster": clusters
    })

def get_similarity(features):
    similarity = {}
    similarity["e_mails"] = calculate_similarity(features["e_mails"], 1, "text", features["countries"], True)
    similarity["affiliations"] = calculate_similarity(features["affiliations"], 0.60, "fuzzy", features["countries"])
    similarity["authors"] = calculate_similarity(features["authors"], 1, "list", features["countries"])
    #similarity["texts"] = calculate_similarity(features["texts"], 0.75, "dict", features["countries"])
    similarity["mesh"] = calculate_similarity(features["mesh"], 3, "list", features["countries"])

    return similarity

def find_clusters(job):
    global matrix
    matrix = librarian.get_matrix((job["request"]["author_id"]))
    features = get_features(matrix)
    similarity = get_similarity(features)

    graph = nx.Graph()
    for pmid in matrix["article_features"]:
        graph.add_node(pmid)

    build_graph(graph, similarity["e_mails"])
    build_graph(graph, similarity["affiliations"])
    build_graph(graph, similarity["authors"])
    #build_graph(graph, similarity["texts"])
    build_graph(graph, similarity["mesh"])

    clusters = find_groups(graph)

    save_model(similarity, clusters, job)

if __name__ == "__main__":
    pass
