import networkx as nx
import mini_nlp
import librarian

def build_graph(graph, similarity):
    for pmid1 in similarity:

        for pmid2 in similarity[pmid1]["scores"]:
            if not graph.has_edge(pmid1, pmid2) and similarity[pmid1]["scores"][pmid2]["include"]:
                graph.add_edge(pmid1, pmid2)

def find_groups(graph):
    connected_components = sorted(nx.connected_components(graph))
    clusters = {}
    clusters.setdefault("articles", {})

    for group_id in range (0, len(connected_components)):
        for pmid in connected_components[group_id]:
            clusters["articles"][pmid] = (group_id + 1)

    clusters.setdefault("summary", {})
    clusters["summary"] = len(connected_components)

    return clusters

def calculate_similarity(features, threshold, calc_type):
    similarity = {}
    for pmid1 in features:
        similarity.setdefault(pmid1,{})
        similarity[pmid1].setdefault("scores",{})
        for pmid2 in features:
            if pmid1 != pmid2 and features[pmid1] and features[pmid2]:
                if calc_type == "string":
                    score = mini_nlp.calc_cosine(features[pmid1], features[pmid2])
                elif calc_type == "list":
                    score = 1 if any([features[pmid1] in a for features[pmid2] in b]) else 0

                similarity[pmid1]["scores"][pmid2] = {
                    "score": score,
                    "include": score >= threshold
                }
                #similarity[pmid1]["debug"][pmid2] = features[pmid2]
    return similarity

def get_features(matrix):
    affiliations, e_mails, authors = {}, {}, {}

    for pmid in matrix["article_features"]:

        if "e_mail" in matrix["article_features"][pmid]:
            e_mails[pmid] = matrix["article_features"][pmid]["e_mail"]

        if "affiliation" in matrix["article_features"][pmid]:
            affiliations[pmid] = matrix["article_features"][pmid]["affiliation"]

        if "authors" in matrix["article_features"][pmid]["authors"]:
            if len(matrix["article_features"][pmid]["authors"]) > 0:
                authors[pmid] = matrix["article_features"][pmid]["authors"]

    return {
        "affiliation": affiliations,
        "authors": authors,
        "e_mail": e_mails
    }

def save_model(similarity, clusters, job):
    librarian.save_relic({
        "_id": job["request"]["author_id"],
        "author": job["request"]["first_name"] + " " + job["request"]["last_name"],
        "similarity": similarity
    })
    librarian.save_clusters({
        "_id": job["request"]["author_id"],
        "author": job["request"]["first_name"] + " " + job["request"]["last_name"],
        "cluster": clusters
    })

def get_similarity(features):
    similarity = {}
    similarity["affiliation"] = calculate_similarity( features["affiliation"], 0.6, "string")
    similarity["e_mail"] = calculate_similarity( features["e_mail"], 1, "string")
    similarity["authors"] = calculate_similarity( features["authors"], 1, "list")
    return similarity

def find_clusters(job):
    matrix = librarian.get_matrix((job["request"]["author_id"]))
    features = get_features(matrix)
    similarity = get_similarity(features)

    graph = nx.Graph()
    for pmid in features:
        graph.add_node(pmid)

    for feature_set in similarity:
        build_graph(graph, similarity[feature_set])

    clusters = find_groups(graph)

    save_model(similarity, clusters, job)

if __name__ == "__main__":
    pass
