import librarian

if __name__ == "__main__":

    f = open("C:\my\clusters.txt", "w")
    f.write("author_id\tauthor_name\tpmid\tgroup_id\tfirst_name\tlast_name\tfound_affiliation\taffiliation_source\tyear\n")
    for author_id in librarian.get_authors_with_clusters():
        print author_id
        cluster = librarian.get_clusters_for_author(author_id)
        if cluster and "cluster" in cluster and "articles" in cluster["cluster"]:
            for pmid in cluster["cluster"]["articles"]:
                article_info = librarian.get_clusters_for_author_id_pmid(author_id, pmid)
                output = []
                output.append(author_id)
                output.append(cluster["author"])
                output.append(pmid)
                output.append(str(cluster["cluster"]["articles"][pmid]))
                output.append(article_info["match"]["first_name"])
                output.append(article_info["match"]["last_name"])
                if article_info["match"]["affiliation"]:
                    output.append(article_info["match"]["affiliation"])
                    output.append("Author")
                elif article_info["article"]["authors"][0]["affiliation"]:
                        output.append(article_info["article"]["authors"][0]["affiliation"])
                        output.append("Article")
                else:
                    output.append("-")
                    output.append("NA")
                output.append(str(article_info["article"]["year"]))
                f.write("\t".join(output) + "\n")
    f.close()
    print "done!"