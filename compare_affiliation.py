import mini_nlp
import affiliation_extractor
import re
import table_reader
import io
import librarian
from collections import OrderedDict
from statistics import mean
from math import log
import sys

group_keys = {}
groups = {}

def group_maker(group_id):
    global group_keys

def no_punc(s):
    return re.sub(r"['_,!\-\"\\\/}{?\.\*\|\(\)\+\;]", ' ', s).strip().replace("  ", " ")

def scoring(article_affiliation_a, research_affiliation_b):
    if research_affiliation_b != "(na)":
        research_affiliation_b = no_punc(affiliation_extractor.clean_affiliation(research_affiliation_b.lower()))
        research_affiliation_parts = research_affiliation_b.split()

        if article_affiliation_a  != "(na)":
            article_affiliation_a = no_punc(affiliation_extractor.clean_affiliation(article_affiliation_a.lower()))
            cosine = mini_nlp.calc_cosine_on_text(article_affiliation_a, research_affiliation_b)
            article_affiliation_parts = article_affiliation_a.split()
            matches = 0

            for j in range(0, len(research_affiliation_parts)):
                for k in range(0, len(article_affiliation_parts)):
                    if research_affiliation_parts[j] in article_affiliation_parts[k]:
                        matches += 1
                        break
            return int(round(cosine * 100, 0)), matches, len(research_affiliation_parts)
        else:
            return -1, -1, len(research_affiliation_parts)
    else:
        return -1, -1, -1

def highlight(article_affiliation_a, research_affiliation_b):
    if article_affiliation_a == "(na)" or research_affiliation_b == "(na)":
        return article_affiliation_a

    research_words = no_punc(affiliation_extractor.clean_affiliation(research_affiliation_b.lower())).split()
    result = no_punc(affiliation_extractor.clean_affiliation(article_affiliation_a.lower()))

    for a in research_words:
        try:
            result = re.sub("\\b" + re.escape(a) + "\\b",  re.escape(a.upper()), result) #result.replace(a, "*" + a.upper() +"*")
        except:
            print "Error in highligt for: " + a
    return result

def get_header(row):
    header = []
    if i == 0:
        for key in row.keys():
            if key == "pmid":
                header.append(key)
                header.append("url")
                header.append("keep (y/n)")
            else :
                header.append(key)
        header.append("affiliation_hits_score")
        header.append("primary_affiliation")
    return header

def make_group_id(author_id):
    global group_keys
    if author_id not in group_keys:
        if len(group_keys) == 0:
            group_keys[author_id] = 1
        else:
            group_keys[author_id] = 1 + max([val for val in group_keys.values()])
    return group_keys[author_id]

def aggregate_feature(counts, article_features):
    for key in article_features:
        counts.setdefault(key, 0)
        counts[key] += article_features[key]

def update_scalar_count(counts, article_features, feature_key):
    if feature_key in article_features:
        key = article_features[feature_key]
        counts.setdefault(key, 0)
        counts[key] += 1

def get_group_features(author_id, pmids):
    matrix = librarian.get_matrix(author_id)
    affiliation_counts = {}
    author_counts = {}
    journal_counts = {}
    mesh_counts = {}
    text_counts = {}
    country_counts = {}

    if "article_features" in matrix:
        for article in matrix["article_features"]:
            if article in pmids:
                update_scalar_count(affiliation_counts, matrix["article_features"][article], "affiliation")
                update_scalar_count(country_counts, matrix["article_features"][article], "country")
                aggregate_feature(journal_counts, matrix["article_features"][article]["journal"])
                aggregate_feature(author_counts, matrix["article_features"][article]["authors"])
                aggregate_feature(mesh_counts, matrix["article_features"][article]["mesh"])
                aggregate_feature(text_counts, matrix["article_features"][article]["text"])

    summary = OrderedDict()
    summary["affiliations"] = ";* ".join(get_top(affiliation_counts, 3))
    summary["authors"] = ";* ".join(get_top(author_counts,5))
    summary["journals"] = ";* ".join(get_top(journal_counts,3))
    summary["countries"] = ";* ".join(get_top(country_counts,3))
    summary["mesh"] = ";* ".join(get_top(mesh_counts, 5))
    summary["text"] = ";* ".join(get_top(text_counts, 5))

    return summary

def get_top(counts, n):
    return sorted(counts, key=counts.get, reverse=True)[:n]


if __name__ == "__main__":
	
    if len(sys.argv) != 3:
    	print "Missing command-line arguments:"
        print " arg 1: authors_file (input file to author.explorer.py" 
        print " arg 2: pubs_file (from article_exporter.py)" 
        exit()

    authors_file = table_reader.read_table(sys.argv[1], "\t", "utf-8")
    pubs_file = table_reader.read_table(sys.argv[2], "\t", "utf-8")

    output = io.open("exported_scored_pubs.txt", "w", 1, "utf-8")
    group_out = io.open("exported_groups.txt", "w", 1, "utf-8")

    i = 0

    for row in pubs_file:
        author_group_id = None
        if i == 0:
            output.write("\t".join(get_header(row)) + "\n")

        out_list = []

        article_affiliation = row["found_affiliation"].strip()

        researched_affiliation_list = [author["affiliation"].strip()
                                  for author
                                  in authors_file if author["author_id"] == row["author_id"]]

        if researched_affiliation_list:
            researched_affiliation = researched_affiliation_list[0]
        else:
            researched_affiliation = "(na)"

        #print researched_affiliation

        for key in row:
            if key == "group_id":
                if row[key] != "?":
                    author_key_id = "G%03d" % make_group_id(row["author_id"])
                    group_id = "%03d" % int(row[key])
                    author_group_id = author_key_id + "-" + group_id
                    out_list.append(author_group_id)
                else:
                    out_list.append(author_key_id + "-?")
            elif key == "pmid":
                out_list.append(row[key])
                out_list.append("http://www.ncbi.nlm.nih.gov/pubmed/" + row[key])
                out_list.append("?")
            elif key == "found_affiliation":
                out_list.append(highlight(article_affiliation, researched_affiliation))
            else:
                out_list.append(row[key])


        cosine, hits, out_of = scoring(article_affiliation, researched_affiliation)

        if out_of > 0 and hits > -1:
            out_list.append(str(int(min(100, round(10 * hits / log(1 + out_of, 10), 0)))))
            #out_list.append(str(hits) + " of " + str(out_of))
        else:
            out_list.append("-1")
            #out_list.append("-")
        out_list.append(researched_affiliation)
        output.write(u"\t".join(out_list) + "\n")

        groups.setdefault(author_group_id, {})
        groups[author_group_id].setdefault("author_id", row["author_id"])
        groups[author_group_id].setdefault("author_name", row["author_name"])
        groups[author_group_id].setdefault("hits", [])
        groups[author_group_id].setdefault("found_names", [])
        #groups[author_group_id].setdefault("out_of", out_of)
        groups[author_group_id].setdefault("pmids", [])
        groups[author_group_id].setdefault("years", [])
        groups[author_group_id]["pmids"].append(row["pmid"])
        groups[author_group_id]["years"].append(row["year"])
        if row["found_name"] not in groups[author_group_id]["found_names"]:
            groups[author_group_id]["found_names"].append(row["found_name"])

        if out_of > 0 and hits > 0:
            groups[author_group_id]["hits"].append(int(min(100, round(10 * hits / log(1 + out_of, 10), 0))))
        #else:
        #    groups[author_group_id]["hits"] = "-1"

        #if hits > -1:
        #    groups[author_group_id]["hits"].append(int(min(100, 10 * round(hits / log(1 + out_of, 10)), 0)))
        #else:
        #    groups[author_group_id]["hits"].append(0)

        groups[author_group_id].setdefault("primary_affiliation", researched_affiliation)
        i += 1

    output.close()

    header = [
        u"author_group_id",
        u"author_id",
        u"author_name",
        u"found_names",
        u"articles_in_group",
        u"affiliation_hits_score",
        u"primary_affiliation",
        u"found_affiliations",
        u"found_co_authors",
        u"found_journals",
        u"found_countries",
        u"found_topics",
        u"found words",
        u"min_year",
        u"max_year"

    ]

    group_out.write("\t".join(header) + "\n")

    for author_group_id in groups:
        gl = []
        group = groups[author_group_id]
        gl.append(author_group_id)
        gl.append(group["author_id"])
        gl.append(group["author_name"])
        gl.append(";* ".join(group["found_names"]))
        gl.append(str(len(group["pmids"])))

        if len(group["hits"]) > 0:
            gl.append(str(round(mean(group["hits"]), 1))) # + " of " + str(group["out_of"]))
        else:
            gl.append("-1")

        gl.append(group["primary_affiliation"])
        features = get_group_features(group["author_id"], groups[author_group_id]["pmids"])
        for feature in features:
            if len(features[feature]) > 0:
                if feature == "affiliations":
                    gl.append(highlight(features[feature], group["primary_affiliation"]))
                else:
                    gl.append(features[feature])
            else:
                gl.append("-")

        gl.append(str(min(group["years"])))
        gl.append(str(max(group["years"])))

        group_out.write("\t".join(gl) + "\n")

    group_out.close()

    print "compare_affiliations.py - done"
