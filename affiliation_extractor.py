import re
from data_cleanser import *

_affiliation_noise_words = ["center", "centre", "college", "department", "division", "faculty", "hospital", "institute",
                            "medical", "medicine","research","school","unit","teaching", "university",
                            "author's", "author", "authors", "affiliation"]

_affiliation_noise_pattern = re.compile(r'\b(' + r'|'.join(_affiliation_noise_words) + r')\b\s*')

def remove_affiliation_noise_words(affiliation):
    affiliation = affiliation.replace(".", "")
    affiliation = remove_noise_words(_affiliation_noise_pattern.sub(" ", affiliation))
    affiliation = re.sub("\s+"," ", affiliation)
    return affiliation.replace(" ,",",").strip().lower()

def clean_affiliation(affiliation):
    affiliation = remove_noise_words(affiliation)
    affiliation = remove_affiliation_noise_words(affiliation)
    if len(affiliation) < 4:
        return affiliation

    elif affiliation[0:3] in ["1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th"]:
        return affiliation[3:].strip()

    elif re.match("\A\([0-9]\)", affiliation): #(1)Pharmacy
        return affiliation[3:].strip()

    elif affiliation[0] == "*": #*Centre Leon Berard
        return affiliation[1:].strip()

    elif affiliation[0] in (".", ":"):
        return affiliation[1:].strip()

    elif affiliation[0] == "*": #*Department of Gynecology
        return affiliation[1:].strip()

    elif re.match("\A[0-9][0-9]?\]", affiliation): #1] Department of Oncology
        return re.sub("\A[0-9][0-9]?\]","", affiliation).strip()

    elif re.match("\A[0-9]\.", affiliation): #1. Department of Oncology
        return re.sub("\A[0-9][0-9]?\.","", affiliation).strip()

    elif re.match("\A[0-9][0-9]?\s", affiliation): #1 Department of Oncology
        return re.sub("\A[0-9][0-9]?\s","", affiliation).strip()

    elif re.match("\A[a-z]\.?\s", affiliation): #a Department of Agricultural
        return re.sub("\A[a-z]\.?\s","", affiliation).strip()

    elif re.match("\A[0-9][A-Z]", affiliation): #1Department of Pathology
        return affiliation[1:].strip()

    return affiliation.strip()

def extract_email(affiliation):
    email_regex = re.compile(r'[\w\-][\w\-\.]+@[\w\-][\w\-\.]+[a-zA-Z]{1,4}')
    found_emails = email_regex.findall(affiliation)
    if found_emails:
        affiliation = re.sub(found_emails[0], "", affiliation)
        return affiliation, found_emails[0]
    else:
        return affiliation, None

def get_affiliation_phrases(affiliation):
    return 	[s.strip().strip(".").strip(";") for s in affiliation.split(", ") if len(s) > 1]

def extract_affiliation_and_email(affiliation):
    if affiliation:
        affiliation = clean_affiliation(affiliation).lower()
        affiliation, e_mail = extract_email(affiliation)
        #affiliation_phrases = extract_noun_phrases(affiliation)
        return affiliation, e_mail
    else:
        return [], None

