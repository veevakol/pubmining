import table_reader
import io
rows = table_reader.read_table(r"C:\my\christina_projects_breakup.txt", "\t")

files = {}

for row in rows:
    file_name = row["file"] + ".txt"
    if file_name not in files:
        files[file_name] = io.open(r"C:\my\ch_" + file_name, "w", 1)
        files[file_name].write("\t".join(row.keys()) + "\n")
    files[file_name].write("\t".join(row.values()) + "\n")


for f in files.values():
    f.close()
