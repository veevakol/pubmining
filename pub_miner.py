# -*- coding: UTF-8 -*-
import request_maker
from BeautifulSoup import BeautifulSoup
import librarian
from article import Article
#from collections import OrderedDict
#from datetime import datetime

_max_authors = 50

def __chunk_pmids (my_list, chunk_pmidss):
    for i in xrange(0, len(my_list), chunk_pmidss):
        yield my_list[i:i + chunk_pmidss]

def __get_article(soup_article):
    return Article(soup_article).get_article_dict()

def mine_articles(pmid_list):
    target_pmids = librarian.get_articles_not_in_mongodb(pmid_list)
    if target_pmids:
        for pmids in __chunk_pmids(target_pmids, 500):
            articles_xml = request_maker.execute_url("http://www.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=Pubmed&retmode=xml&id=%s" % str.join(",", pmids))
            soup = BeautifulSoup(articles_xml)
            soup_articles = soup.findAll(name="pubmedarticle")
            articles = []
            for soup_article in soup_articles:
                articles.append(__get_article(soup_article))
            librarian.save_articles(articles)
    return len(pmid_list), len(target_pmids)


if __name__ == "__main__":
    mine_articles(["11480952","22827722","17728712"])
