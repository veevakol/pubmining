import sys
import io
import librarian
import HTMLParser

def chunk(my_list, chunks):
	for i in xrange(0, len(my_list), chunks):
		yield my_list[i:i + chunks]

if __name__ == "__main__":
    pmids_file = io.open(sys.argv[1], "r", 1, "utf-8") #must be one PMID per line; no header
    output_file = io.open(sys.argv[2], "w", 1, "utf-8")
    h = HTMLParser.HTMLParser()

    lines = pmids_file.readlines()
    pmids_file.close()
    all_pmids = [p.strip() for p in lines]

    pmids = list(set(all_pmids))
    print "*" * 20
    print "Total PMIDs: " + str(len(all_pmids))
    if len(all_pmids) != len(pmids):
        print "Distinct PMIDs: " + str(len(pmids))
    print "*" * 20

    output_file.write(u"pmid\ttitle\tyear\tjournal\tabstract\n")
    i = 0

    for chunks in chunk(pmids, 50):
        articles = librarian.get_articles(chunks)
        for article in articles:
            if article:
                i += 1
                print article["_id"] + " -- " + str(i) + " -- " + str(int(round((i * 100.0) / len(pmids), 0))) + "%"
                output_file.write(article["_id"])
                output_file.write(u"\t")
                output_file.write(h.unescape(article["title"]))
                output_file.write(u"\t")
                output_file.write(unicode(article["year"]))
                output_file.write(u"\t")
                output_file.write(h.unescape(article["journal"]))
                output_file.write(u"\t")
                output_file.write(h.unescape(article["abstract"]))
                output_file.write(u"\n")

    output_file.close()

    print "Done"