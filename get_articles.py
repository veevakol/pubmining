import librarian

f = open(r"C:\my\388.txt", "r")
o = open(r"C:\my\388_out.txt", "w")

pmids = f.read().strip().split("\n")

articles = librarian.get_articles(pmids)

for a in articles:
	if a:
		print a
		o.write(a["_id"])
		o.write("\t")
		o.write(a["abstract"])
		o.write("\n")

f.close()
o.close()

print "done"

		