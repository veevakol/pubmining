# -*- coding: UTF-8 -*-
import sys
import re
import codecs
import urllib
from multiprocessing import Process, Queue
import pub_miner
from dateutil import parser as dateparser  # python-dateutil
from BeautifulSoup import BeautifulSoup
import request_maker
import librarian
import featurer_finder
from humpty_dumpy_config import *
import cluster_maker
import table_reader
import data_cleanser

def get_match_against_text(candidate_author):
    return data_cleanser.remove_extra_spaces(unicode(
        candidate_author["first_name"] + " " + candidate_author["last_name"]))

def evaluate_author_pub(regex_test_pattern, article):
    candidate_authors = [author for author in article["authors"] if author["is_person"] == 1]
    for candidate_author in candidate_authors:
        match_against = get_match_against_text(candidate_author)
        if regex_test_pattern == "" or re.match(regex_test_pattern, match_against, re.IGNORECASE):
            return candidate_author
    return None

def link_author_pubs(job):
    articles_list = librarian.get_articles(job["response"]["pmids"])
    precursors_list = []
    #i = 0
    #perc = 0
    for article in articles_list:
        #i = i + 1
        #lastperc = perc
        #perc = str(int(round((i * 100.0) / len(articles_list))))
        #if perc != lastperc:
        #    print perc
        matched_author = evaluate_author_pub(job["request"]["test_pattern"], article)
        if matched_author:
            precursors_list.append({
                "_id": article["_id"] + "$" + job["request"]["author_id"],
                "pmid": article["_id"],
                "author_id": job["request"]["author_id"],
                "author_first_name": job["request"]["first_name"],
                "author_last_name": job["request"]["last_name"],
                "match": matched_author,
                "article": article
            })
    librarian.save_precursors(precursors_list)

def build_pmid_query(job):
    query_list = []

    if job["request"]["pubmed_author_query"]:
        query_list.append("(" + job["request"]["pubmed_author_query"] + ")")

    if job["request"]["earliest_search_date"]:
        query_list.append("(\"" + job["request"]["earliest_search_date"].strftime("%Y-%m-%d") +
                          "\"[PDat] : \"3000/01/01\"[PDat])")

    if job["request"]["domain_query"].strip() not in ["", "NO_VALUE"]:
        query_list.append("(" + job["request"]["domain_query"] + ")")

    dict = {}
    dict["term"] = "(" + str.join(" AND ", query_list) + "))"
    dict["retmode"] = "xml"
    dict["usehistory"] = "n"
    dict["db"] = "pubmed"
    dict["retmax"] = retmax
    query = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?%s" % urllib.urlencode(dict)

    return query

def get_author_pmids(job):
    query = build_pmid_query(job)
    soup = BeautifulSoup(request_maker.execute_url(query))
    soup_ids = soup.findAll(name="id")
    author_pmids = [id.text for id in soup_ids]
    return author_pmids

def mine_authors(jobs_list):
    while not jobs_list.empty():
        job = jobs_list.get()
        job.setdefault("response", {})
        job["response"].setdefault("pmids", get_author_pmids(job))

        if job["domain_pmids"]:
            job["response"]["pmids"] = set(job["response"]["pmids"]).intersection(job["domain_pmids"])

        x = job["response"]["pmids"]
        pmid_list_count, target_pmids_count = pub_miner.mine_articles(x)

        print str(job["index"]) + ". " + job["request"]["first_name"] + " " + job["request"]["last_name"] \
              + ": match:" + str(pmid_list_count) + "; downloaded:" + str(target_pmids_count)

        link_author_pubs(job)

        if not job["mine_pubs_only"]:
            featurer_finder.create_matrix(job)
            cluster_maker.find_clusters(job)

def manage_author_mining_jobs(author_requests, published_after_date, pubmed_cutoff_date, domain_pmids,
                              concurrently, mine_pubs_only):

    jobs_list = create_jobs_list(author_requests, domain_pmids, published_after_date,
                                 pubmed_cutoff_date, mine_pubs_only)
    if concurrently:
        processes = []

        #for i_process in range(0, max_processes):
        for i_process in range(0, 8):
            proc = Process(target=mine_authors, args=(jobs_list,))
            processes.append(proc)
            proc.start()

        for proc in processes:
            proc.join()

    else:
        mine_authors(jobs_list)

def create_jobs_list(author_requests, domain_pmids, published_after_date, pubmed_cutoff_date, mine_pubs_only):
    index = 0
    jobs_list = Queue()
    for author in author_requests:
        index += 1
        job = {
            "index": index,
            "request": author,
            "published_after_date": dateparser.parse(published_after_date),
            "pubmed_cutoff_date": dateparser.parse(pubmed_cutoff_date),
            #"pubmed_author_query": author["pubmed_author_query"],
            #"domain_query": author["domain_query"],
            "domain_pmids": domain_pmids,
            "mine_pubs_only": mine_pubs_only
        }
        job["request"]["earliest_search_date"] = dateparser.parse(min([published_after_date, pubmed_cutoff_date]))
        jobs_list.put(job)
    return jobs_list

def load_author_request_file(author_request_file_path):
    author_requests = []
    #f_in = codecs.open(author_request_file_path, "r", encoding="utf-8")
    #f = open(author_request_file_path, "r")
    #rows = [row.strip() for row in f.readlines()[1:]]
    rows = table_reader.read_table(author_request_file_path, "\t", "utf-8")
    for row in rows:
        author_requests.append({
            "author_id": row["author_id"],
            "first_name": row["first_name"],
            "middle_name": row["middle_name"],
            "last_name": row["last_name"],
            "pubmed_author_query": row["pubmed_author_query"],
            "test_pattern": row["test_pattern"],
            "domain_query": row["domain_query"],
            "affiliation": row["affiliation"],
            "country": row["country"],
            "use_strict_query": row["use_strict_query"]
        })

    return author_requests

def mine_authors_in_file(author_request_file_path,
                 published_after_date="1995-1-1",
                 pubmed_cutoff_date="3000-01-01",
                 domain_input_file_path=None,
                 mine_pubs_only=False):

    author_requests = load_author_request_file (author_request_file_path)
    domain_pmids = []
    if domain_input_file_path:
        domain_file = codecs.open(domain_input_file_path, "r", "utf-8")
        domain_pmids = [line.strip() for line in domain_file.readlines()]
        domain_file.close()

    manage_author_mining_jobs(author_requests, published_after_date, pubmed_cutoff_date, domain_pmids, concurrently=True, mine_pubs_only=mine_pubs_only)

if __name__ == "__main__":
    print "author_explorer - Started " + "*" * 10
    librarian.reset_workspace()
    mine_authors_in_file (author_request_file_path=sys.argv[1])
    print "author_explorer - Done " + "*" * 10
