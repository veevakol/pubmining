"""
critics={
'Lisa Rose': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.5, 'Just My Luck': 3.0, 'Superman Returns': 3.5, 'You, Me and Dupree': 2.5, 'The Night Listener': 3.0},
'Gene Seymour': {'Lady in the Water': 3.0, 'Snakes on a Plane': 3.5, 'Just My Luck': 1.5, 'Superman Returns': 5.0, 'The Night Listener': 3.0, 'You, Me and Dupree': 3.5}, 
'Michael Phillips': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.0, 'Superman Returns': 3.5, 'The Night Listener': 4.0},
'Claudia Puig': {'Snakes on a Plane': 3.5, 'Just My Luck': 3.0, 'The Night Listener': 4.5, 'Superman Returns': 4.0,  'You, Me and Dupree': 2.5},
'Mick LaSalle': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0, 'Just My Luck': 2.0, 'Superman Returns': 3.0, 'The Night Listener': 3.0, 'You, Me and Dupree': 2.0}, 
'Jack Matthews': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0, 'The Night Listener': 3.0, 'Superman Returns': 5.0, 'You, Me and Dupree': 3.5},
'Toby': {'Snakes on a Plane':4.5,'You, Me and Dupree':1.0,'Superman Returns':4.0}}
"""

from math import sqrt

# Returns a distance-based similarity score for pub1 and pub2
def sim_distance(features, pub1, pub2):
  # Get the list of shared_items
  si={}
  for item in features[pub1]: 
    if item in features[pub2]: si[item]=1

  # if they have no ratings in common, return 0
  if len(si)==0: return 0

  # Add up the squares of all the differences
  sum_of_squares=sum([pow(features[pub1][item]-features[pub2][item],2) 
                      for item in features[pub1] if item in features[pub2]])

  return 1/(1+sum_of_squares)

# Returns the Pearson correlation coefficient for pub1 and pub2
def sim_pearson(features, pub1, pub2):
  # Get the list of mutually rated items
  si={}
  for item in features[pub1]: 
    if item in features[pub2]: si[item]=1

  # if they are no ratings in common, return 0
  if len(si)==0: return 0

  # Sum calculations
  n=len(si)
  
  # Sums of all the preferences
  sum1=sum([features[pub1][it] for it in si])
  sum2=sum([features[pub2][it] for it in si])
  
  # Sums of the squares
  sum1Sq=sum([pow(features[pub1][it],2) for it in si])
  sum2Sq=sum([pow(features[pub2][it],2) for it in si])	
  
  # Sum of the products
  pSum=sum([features[pub1][it]*features[pub2][it] for it in si])
  
  # Calculate r (Pearson score)
  num=pSum-(sum1*sum2/n)
  den=sqrt((sum1Sq-pow(sum1,2)/n)*(sum2Sq-pow(sum2,2)/n))
  if den==0: return 0

  r=num/den

  return r

# Returns the best matches for pub from the features dictionary. 
# Number of results and similarity function are optional params.
def topMatches(features, pub, n=5, similarity=sim_pearson):
  scores=[(similarity(features,pub,other),other) 
                  for other in features if other!=pub]
  scores.sort()
  scores.reverse()
  return scores[0:n]

# Gets recommendations for a pub by using a weighted average
# of every other user's rankings
def getRecommendations(features, pub, similarity=sim_pearson):
  totals={}
  simSums={}
  for other in features:
    # don't compare me to myself
    if other==pub: continue
    sim=similarity(features,pub,other)

    # ignore scores of zero or lower
    if sim<=0: continue
    for item in features[other]:
	    
      # only score movies I haven't seen yet
      if item not in features[pub] or features[pub][item]==0:
        # Similarity * Score
        totals.setdefault(item,0)
        totals[item]+=features[other][item]*sim
        # Sum of similarities
        simSums.setdefault(item,0)
        simSums[item]+=sim

  # Create the normalized list
  rankings=[(total/simSums[item],item) for item,total in totals.items()]

  # Return the sorted list
  rankings.sort()
  rankings.reverse()
  return rankings

def transformfeatures(features):
  result={}
  for pub in features:
    for item in features[pub]:
      result.setdefault(item,{})
      
      # Flip item and pub
      result[item][pub]=features[pub][item]
  return result


def calculateSimilarItems(features, n=10):
  # Create a dictionary of items showing which other items they
  # are most similar to.
  result={}
  # Invert the preference matrix to be item-centric
  itemfeatures=transformfeatures(features)
  c=0
  for item in itemfeatures:
    # Status updates for large datasets
    c+=1
    if c%100==0: print "%d / %d" % (c,len(itemfeatures))
    # Find the most similar items to this one
    scores=topMatches(itemfeatures,item,n=n,similarity=sim_distance)
    result[item]=scores
  return result

def getRecommendedItems(features, itemMatch, user):
  userRatings=features[user]
  scores={}
  totalSim={}
  # Loop over items rated by this user
  for (item,rating) in userRatings.items( ):

    # Loop over items similar to this one
    for (similarity,item2) in itemMatch[item]:

      # Ignore if this user has already rated this item
      if item2 in userRatings: continue
      # Weighted sum of rating times similarity
      scores.setdefault(item2,0)
      scores[item2]+=similarity*rating
      # Sum of all the similarities
      totalSim.setdefault(item2,0)
      totalSim[item2]+=similarity

  # Divide each total score by total weighting to get an average
  rankings=[(score/totalSim[item],item) for item,score in scores.items( )]

  # Return the rankings from highest to lowest
  rankings.sort( )
  rankings.reverse( )
  return rankings

