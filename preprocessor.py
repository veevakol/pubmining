# -*- coding: UTF-8 -*-
import sys
import codecs
import table_reader
import accent_remover
import re

reload(sys)
sys.setdefaultencoding('utf8')
UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)


def replace_misc(s):
    return s.replace(" ", "\s?").replace("-", ".?").replace("'", ".?")

def no_value_if_empty(s):
    if not s or s.strip() == "":
        return "NO_VALUE"
    else:
        return s.strip()

def get_first_name_pattern(name):
    name = name.replace(".", "")
    if len(name) == 0:
        return "([\w\s\.'-]*)?"
    elif len(name) == 1:
        return name + "([\w\s\.'-]*)?"
    elif "-" in name:
        initial = re.sub(r'.+\-(\w).+', r'\1', name)
        pattern = "\A" + name[0] + initial + "?" + replace_misc("(" + name[1:] + ")?\s+")
        return pattern
    elif " " in name:
        nameshort = re.sub(r'\s.+', '', name)
        init2 = re.sub(r'.+\s(\w).+', r'\1', name)
        pattern = "\A" + name[0] + replace_misc("(" + name[1:] + "|" + nameshort[1:] + "| " + init2 + ")?\s+")
        return pattern
    else:
        pattern = "\A" + name[0] + "(\w.+)?" + replace_misc("(" + name[1:] + ")?\s+")
        return pattern

def get_middle_name_pattern(name):
    name = name.replace(".", "")
    split = name.split(" ")
    if len(split) > 1 and len(split[1]) == 1:
        name = split[0]

    if len(name) == 0:
        return "([\w\s\.'-]*)?"
    elif len(name) == 1:
        return "(" + name + "([\w\s\.'-]*)?)?"
    else:
        result = "(" + name[0] + "(" + replace_misc(name [1:])  +  ")?" + "(\s([\w\s\.'-]*))?" + ")?"
    return result

def get_last_name_pattern(last_name):
    last_name = last_name.replace(".", "").split(",")[0]
    if re.search(r"\W", last_name):
        lnames = re.sub(r"\W+", "|", last_name)
        return "\\b(" + replace_misc(last_name) + "|" + lnames + ")\\b"
    else:
        return "\\b(" + replace_misc(last_name) + ")\\b"

def get_name_pattern(first_name, middle_name, last_name):

    first_name_pattern = get_first_name_pattern(first_name)
    middle_name_pattern = get_middle_name_pattern(middle_name)
    last_name_pattern = get_last_name_pattern(last_name)

    output_array = []

    output_array.append(first_name_pattern)
    output_array.append(middle_name_pattern)
    output_array.append(last_name_pattern)

    result = "".join(output_array)

    return result

def get_pubmed_author_query(first_name, middle_name, last_name, use_strict_query):

    l_list = [last_name]

    if " " in last_name:
        l_list.append(last_name.replace(" ", ""))
        l_list.extend(re.split(r"\W+", last_name))

    if "'" in last_name:
        l_list.append(last_name.replace("'", ""))

    if "-" in last_name:
        l_list.append(last_name.replace("-", ""))
        l_list.append(last_name.replace("-", " "))
        l_list.extend(re.split(r"\W+", last_name))

    f = first_name[0]
    m = "" if len(middle_name) == 0 else middle_name[0]

    author_queries = []

    for l in l_list:

        if len(first_name) == 1 and len(middle_name) > 0:
            author_queries.append("\"" + l + " " + f + m + "\"[au]")
            author_queries.append("\"" + l + " " + f + "\"[au]")

        else:

            if (len(middle_name) == 0 and "-" not in first_name) or use_strict_query is False:
                if "-" not in first_name and use_strict_query is False:
                    author_queries.append(l + " " + f + "[au]")
                else:
                    author_queries.append("\"" + l + " " + f + "\"[au]")

            if len(middle_name) > 0:
                author_queries.append("\"" + l + " " + f + m + "\"[au]")

            if len(first_name) > 2 and "-" in first_name and first_name[-1] != "-":
                f_parts = first_name.split("-")
                author_queries.append("\"" + l + " " + f_parts[0][0] + f_parts[1][0].upper() + "\"[au]")

            if len(first_name) > 2 and " " in first_name:
                f_parts = first_name.split(" ")
                author_queries.append("\"" + l + " " + f_parts[0][0] + f_parts[1][0].upper() + "\"[au]")

    #print author_queries

    """
    print use_strict_query,
    print "#",
    print first_name,
    if middle_name:
        print middle_name,
    print last_name,
    print "#"
    print ", ".join(author_queries),
    """

    #author_queries = list(set(author_queries))

    return " OR ".join(author_queries)

def passThroughResult(o):
    return o

def process_input_file(input_file_path, output_file_path):
    output_array = []
    rows = table_reader.read_table(input_file_path, "\t")
    for row in rows:
        author_id = row["author_id"].strip()
        first_name = accent_remover.swap_accents(row["first_name"]).strip()
        middle_name = accent_remover.swap_accents(row["middle_name"]).strip()
        last_name = accent_remover.swap_accents(row["last_name"]).strip()
        affiliation =  no_value_if_empty(accent_remover.swap_accents(row["affiliation"]).strip())
        country = no_value_if_empty(accent_remover.swap_accents(row["country"])).strip()
        domain_query = no_value_if_empty(row["domain_query"]).strip()
        use_strict_query = (True if row["use_strict_query"].strip() == "Y" else False)
        output_array.append(
            {
            "author_id": author_id,
            "first_name": first_name,
            "middle_name": middle_name,
            "last_name": last_name,
            "pubmed_author_query": passThroughResult(get_pubmed_author_query(first_name, middle_name, last_name, use_strict_query)), #get_pubmed_author_query(first_name, middle_name, last_name, use_strict_query),
            "test_pattern": get_name_pattern(first_name, middle_name, last_name),
            "domain_query": domain_query,
            "affiliation": affiliation,
            "country": country,
            "use_strict_query": "Y" if use_strict_query else "N"
            }
        )

    fieldnames = ["author_id", "first_name", "middle_name", "last_name", "pubmed_author_query", "test_pattern", "domain_query", "affiliation",	"country", "use_strict_query"]

    outfile = codecs.open(output_file_path, "wb", "utf-8")
    outfile.write("\t".join(fieldnames) + "\n")
    for rows in output_array:
        for i in range(0, len(fieldnames)):
            fld = fieldnames[i]
            outfile.write(rows[fld])
            outfile.write("\n" if i == len(fieldnames) - 1 else "\t")
    outfile.close()
    return

if __name__ == "__main__":
    process_input_file(sys.argv[1], sys.argv[2])
    print "Done!"
