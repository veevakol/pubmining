# -*- coding: utf-8 -*-
import sys
import codecs
import io
from collections import OrderedDict

reload(sys)

sys.setdefaultencoding('utf8')
UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)

chars = {}
i = 0

f = io.open(sys.argv[1], "r", encoding="cp1252") #encoding="utf-8")
last = OrderedDict()

try:

	for line in f:
	    i += 1
	    print str(i)
	    for ch in line:
	        if ord(ch) > 127:
	            chars.setdefault(ch, 0)
	            chars[ch] += 1
	    last[i] = line 
	f.close()
	a = []

	for k in chars:
	    a.append(k.lower() + "\t" + str(i))
	    a.append(k.upper() + "\t" + str(i))

	o = codecs.open("C:\\my\\accents_found.txt", "w", "cp1252")
	a = sorted(a)
	o.write("\n".join(a))
	o.close()

except Exception, e:
	print "*" * 10
	print "Error on line number " + str(i) 
	print str(e)
	print "line number: " + str(sys.exc_info()[-1].tb_lineno)

	"""
	print "--" * 5
	print last[82]
	print "--" * 5
	print last[83]
	"""
	print "*" * 10

print "Wrote C:\\my\\accents_found.txt"
print "Done!"



