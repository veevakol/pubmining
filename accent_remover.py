# -*- coding: utf-8 -*-
import sys
import codecs
import io
reload(sys)
sys.setdefaultencoding('utf8')
UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)
import io

_accent_alts = None

def swap_accents(text):
    global _accent_alts
    if _accent_alts is None:
        _accent_alts = load_alt_chars()

    if text == None:
        return ""

    output_chars = []

    text_chars = list(text)

    for c in text_chars:
        s = unicode(c)
        if s in _accent_alts:
            output_chars.append(_accent_alts[s])
        else:
            output_chars.append(s)

    results = "".join(output_chars)

    return results

def load_alt_chars():	
    alts = {}
    #f = io.open(r"charmap.txt", "r", 1, "cp1252")
    f = io.open(r"charmap.txt", "r", 1, "utf-8")
    for line in f:
        key, val = line.split("=")
        alts.setdefault(key.strip(), val.strip())
    f.close()
    return alts

