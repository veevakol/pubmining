import csv

class UnicodeDictReader:

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        self.encoding = encoding
        self.reader = csv.DictReader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        try :
            #unicode(value, "utf-8", "ignore")
            #return {k: unicode(v, "utf-8") for k, v in row.iteritems()}
            return {k: unicode(v).decode(encoding="utf-8", errors="ignore") for k, v in row.iteritems()}
        except:
            print "ERROR?"

    def __iter__(self):
        return self
