# -*- coding: UTF-8 -*-
import os
import sys
import re
import codecs
import urllib
from time import strptime
from multiprocessing import Process, Queue
import traceback
import cPickle

from dateutil import parser as dateparser #python-dateutil

from BeautifulSoup import BeautifulSoup
import accent_remover
import data_cleanser
import pub_type_normalizer
import request_maker


def get_pubmed_ids_query(author_query, search_date, content_search):
	query_list = []
	if author_query != "": query_list.append("(" + author_query + ")") 
	if search_date != None: query_list.append("(\"" + search_date.strftime("%Y-%m-%d") + "\"[PDat] : \"2050/01/01\"[PDat])") 
	if content_search.strip() not in ["", "NO_VALUE"]: query_list.append("(" + content_search + ")") 
	dict = {}
	dict["term"] = "(" + str.join(" AND ", query_list) + ")"
	dict["retmode"] = "xml"
	dict["usehistory"] = "n"
	dict["db"] = "pubmed"
	dict["retmax"] = "10000"
	query = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?%s" % urllib.urlencode(dict)
	return query

def get_author_pmid_list(author_query, search_date, content_search):
	query = get_pubmed_ids_query(author_query, search_date, content_search)
	soup = BeautifulSoup(request_maker.execute_url(query))
	soup_ids = soup.findAll(name="id")
	pmids = [id.text for id in soup_ids]
	return pmids

def get_pub_date(soup_pub_date):
	if soup_pub_date:
		pub_date_list = []
		if soup_pub_date.find("year"):
			pub_date_list.append(soup_pub_date.find("year").text)

			if soup_pub_date.find("month"):
				month_text = soup_pub_date.find("month").text
				if unicode(month_text).isnumeric():
					pub_date_list.append(month_text)
				elif len(month_text) >= 3:
					try:
						pub_date_list.append(strptime(month_text[0:3],'%b').tm_mon)
					except:
						pub_date_list.append(month_text)
				else:
					pub_date_list.append(month_text)

				if soup_pub_date.find("day"):
					pub_date_list.append(soup_pub_date.find("day").text)
				else:
					pub_date_list.append("15")

			return dateparser.parse(str.join("-", [str(x) for x in pub_date_list])
	return None

def extract_major_mesh_terms(soup_article):
	mesh_list = []
	mesh_heading_list = soup_article.findAll("meshheading")
	if mesh_heading_list:
		for mesh_section in mesh_heading_list:
			if "majortopicyn=\"y\"" in str(mesh_section).lower():
				mesh_soup = BeautifulSoup(str(mesh_section))
				if mesh_soup:
					for mesh in mesh_soup:
						mesh_list.append(mesh.find("descriptorname").text)

	return "; ".join(mesh_list)

def extract_authors(soup_authors):
	authors = []
	for soup_author in soup_authors:
		author_dict = {}

		if soup_author.find("collectivename") == None:
			
			author_dict["last_name"] = accent_remover.swap_accents(soup_author.find("lastname").text)

			if soup_author.find("forename") != None:
				author_dict["first_name"]  = accent_remover.swap_accents(soup_author.find("forename").text)
			elif soup_author.find("firstname") != None:
				author_dict["first_name"]  = accent_remover.swap_accents(soup_author.find("firstname").text)
			else:
				author_dict["first_name"] = ""

			author_dict["suffix"] = accent_remover.swap_accents(soup_author.find("suffix").text) if soup_author.find("suffix") != None else ""
			author_dict["is_person"] = "Y"

		else:
			
			author_dict["last_name"] = accent_remover.swap_accents(soup_author.find("collectivename").text)
			author_dict["first_name"] = ""
			author_dict["is_person"] = "N"

		author_dict["affiliation"] = data_cleanser.strip_tags(accent_remover.swap_accents(extract_element_text(soup_author, "affiliation"))).encode('ascii',errors='ignore')
		authors.append(author_dict)
	return authors

def extract_element_text(soup_object, element_id):
	if soup_object != None:
		return soup_object.find(element_id).text if soup_object.find(element_id) != None else ""
	return "" 

def chunk(my_list, chunks):
	for i in xrange(0, len(my_list), chunks):
		yield my_list[i:i + chunks]

def process_articles(all_pmids):
	article_list = []
	for pmids in chunk(all_pmids, 500):
		#changing data
		articles_xml = request_maker.execute_url("http://www.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=Pubmed&retmode=xml&id=%s" % str.join(",", pmids))
		soup = BeautifulSoup(articles_xml)
		soup_articles = soup.findAll(name="pubmedarticle")
		for soup_article in soup_articles:
			article_dict = {}
			article_dict["pmid"] = soup_article.find("pmid").text
			article_dict["journal"] = extract_element_text(soup_article, "title")
			if article_dict["journal"] == "":
				article_dict["journal"] = accent_remover.swap_accents(extract_element_text(soup_article, "isoabbreviation"))
			article_dict["affiliation"] = accent_remover.swap_accents(extract_element_text(soup_article, "affiliation"))
			article_dict["title"] = accent_remover.swap_accents(extract_element_text(soup_article, "articletitle"))
			article_dict["type"] = extract_element_text(soup_article, "publicationtype")
			if article_dict["type"] == "":
				article_dict["type"] = "General Article"
			article_dict["status"] = extract_element_text(soup_article, "publicationstatus")
			article_dict["authors"] = extract_authors(soup_article.findAll(name="author"))
			
			article_dict["major_mesh_terms"] = extract_major_mesh_terms(soup_article).replace("\n", " ")
			article_dict["inferred_publication_date"] = get_pub_date(soup_article.find("pubdate"))
			article_dict["pubmed_added_date"] = get_pub_date(soup_article.find("pubmedpubdate",{"pubstatus": "pubmed"}))
			
			if re.match("[0-9]{4}\-[0-9]{1,2}\-{1,2}", article_dict["inferred_publication_date"]):
				article_dict["pub_display_date"] = article_dict["inferred_publication_date"]  
			else:
				article_dict["pub_display_date"] = article_dict["pubmed_added_date"]

			article_dict["abstract"] = data_cleanser.only_ascii(accent_remover.swap_accents(extract_element_text(soup_article, "abstracttext"))).replace("\n", " ")
			if article_dict["abstract"] == "":
				article_dict["abstract"] = data_cleanser.only_ascii(accent_remover.swap_accents(extract_element_text(soup_article, "abstract"))).replace("\n", " ")

			article_list.append(article_dict)
	return article_list

def get_details (desired_author, test_pattern, authors):

	details = {}
	details.setdefault("debug_test_list", [])
	details.setdefault("found", False)

	for i_author in range (0, len(authors)): 
		current_author = authors[i_author]
		match_text = unicode(current_author["first_name"] + " " + current_author["last_name"]).strip().replace("  "," ").replace("'","")

		if desired_author[0].lower() == match_text[0].upper():
			details["debug_test_list"].append(match_text)

		if current_author["is_person"] == "Y" and (test_pattern == "" or re.match(test_pattern, match_text, re.IGNORECASE) != None):
			details["found"] = True
			details ["found_name"] = unicode(current_author["first_name"] + " " + current_author["last_name"] + " " + current_author["suffix"]).strip()

			if i_author == 0:			
				details ["authorship_index"] = "0"
			elif i_author == len(authors) - 1:
				details ["authorship_index"] = "-1"
			else:
				details["authorship_index"] = str(i_author)

			if current_author["affiliation"]:
				details["affiliation"] = current_author["affiliation"] 
				details ["affiliation_source_index"] = details ["authorship_index"]
			elif i_author > 0 and authors[0]["affiliation"]:
				details["affiliation"] = authors[0]["affiliation"]
				details ["affiliation_source_index"] = "0"
			else:
				details["affiliation"] = ""
				details ["affiliation_source_index"] = "null"

			return details 

	return details

def get_co_authors(authors):
	if len(authors) <= 50:
		co_authors = []
		for i in range(0, len(authors)):
			co_authors.append(unicode(authors[i]["first_name"] + " " + authors[i]["last_name"]).strip()) 
		return str.join(", ", co_authors), len(authors)
	else:
		return "", len(authors)

def in_date_range(current_article, pubmed_added_cutoff_date, date_published_cutoff_date):

	#if dateparser.parse(current_article["pubmed_added_date"]) < pubmed_added_cutoff_date: 
	#	return False

	if re.match("[0-9]{4}\-[0-9]{1,2}\-{1,2}", current_article["inferred_publication_date"]):
		if dateparser.parse(current_article["inferred_publication_date"]) < date_published_cutoff_date: 
			return False

	return True 

def output(output_folder, output_files_prefix_queue):
	pub_out = codecs.open(os.path.join(output_folder,"output.txt"), "w", "utf8")
	sum_out = codecs.open(os.path.join(output_folder,"summary.txt"), "w", "utf8")
	hits_out = codecs.open(os.path.join(output_folder,"hits.txt"), "w", "utf8")

	#Columns
	pub_out.write("author_id\tauthor\tfound_name\tauthorship_index\taffiliation_source_index\tpmid\taffiliation\ttitle\tjournal\tstatus\tpub_display_date\tpubmed_added_date\tcoauthors\tcoauthors count\tmesh_terms\tabstract\ttype\tmederi_type\n")
	sum_out.write("author_id\tauthor\tpubs_found\tpubs_matched\n")
	hits_out.write("author_id\tauthor\ttest_pattern\tmatch\tpmid\tauthors\n")

	while not output_files_prefix_queue.empty():

		prefix = output_files_prefix_queue.get()
		pub_in_file_path = prefix + "_pub.txt"
		sum_in_file_path = prefix + "_sum.txt"
		hits_in_file_path = prefix + "_hits.txt"

		if os.path.exists(pub_in_file_path):
			o = codecs.open(pub_in_file_path, "r", "utf8")
			pub_out.write(o.read())
			o.close()
			os.remove(pub_in_file_path)

		if os.path.exists(sum_in_file_path):
			o = codecs.open(sum_in_file_path, "r", "utf8")
			sum_out.write(o.read())
			o.close()
			os.remove(sum_in_file_path)

		if os.path.exists(hits_in_file_path):
			o = codecs.open(hits_in_file_path, "r", "utf8")
			hits_out.write(o.read())
			o.close()
			os.remove(hits_in_file_path)

	pub_out.close()
	sum_out.close()
	hits_out.close()

def print_results(author_id, author, author_test_pattern, articles, prefix_path, pubmed_added_cutoff_date, date_published_cutoff_date):
	pub_out = codecs.open(prefix_path + "_pub.txt", "a", "utf-8")
	sum_out = codecs.open(prefix_path + "_sum.txt", "a", "utf-8")
	hits_out = codecs.open(prefix_path + "_hits.txt", "a", "utf-8")

	sum_dict = {"found": len(articles), "kept": 0}

	for current_article in articles:

		date_analysis = "Y" if in_date_range(current_article, pubmed_added_cutoff_date, date_published_cutoff_date) else "N"
		details = get_details(author, author_test_pattern, current_article["authors"])

		s = []

		hits_out.write(author_id + "\t")
		hits_out.write(author + "\t")
		hits_out.write(author_test_pattern + "\t")
		hits_out.write("N\t" if details == None else "Y\t")
		hits_out.write(current_article["pmid"] + "\t")
		hits_out.write(";".join(details["debug_test_list"]))
		hits_out.write("\n")

		if details["found"]:
			sum_dict["kept"] += 1
			s.append(author_id)
			s.append(author)
			s.append(details["found_name"])
			s.append(details["authorship_index"])
			s.append(details["affiliation_source_index"])
 			s.append(current_article["pmid"])
			s.append(details["affiliation"])
			s.append(current_article["title"])
			s.append(current_article["journal"])
			s.append(current_article["status"])
			s.append(current_article["pub_display_date"])
			s.append(current_article["pubmed_added_date"])
			co_authors, co_authors_count = get_co_authors(current_article["authors"])
			s.append(co_authors)
			s.append(str(co_authors_count))
			s.append(current_article["major_mesh_terms"])
			s.append(current_article["abstract"])
			s.append(current_article["type"])
			s.append(pub_type_normalizer.get_mederi_pub_type(current_article["type"]))
			pub_out.write("\t".join(s) + "\n")

	pub_out.close()
	sum_out.write(author_id + "\t" + author + "\t"  + str(sum_dict["found"]) + "\t" + str(sum_dict["kept"]) + "\n")
	sum_out.close()
	hits_out.close()

	return

def multi_main(pid, dates, output_folder, line_queue, output_files_prefix_queue, domain_pmids_list):
	pubmed_added_cutoff_date = dates[0]
	date_published_cutoff_date = dates[1]
	earliest_search_date = dates[2]

	prefix_path = os.path.join(output_folder, "_" + str(pid)) 
	output_files_prefix_queue.put(prefix_path)

	while not line_queue.empty():
		author_index, line = line_queue.get()
		
		try:
			author_id, author, author_query, author_test_pattern, content_search, target_affiliation, country, use_strict = str(line).split("\t") 
			#print str(pid) + " --> " + author
			author_pmids = get_author_pmid_list(author_query, earliest_search_date, content_search)

			if domain_pmids_list:
				author_pmids = set(author_pmids).intersection(domain_pmids_list)

			pmids = []
			pickled_pmids = []
			for pmid in author_pmids:
				pickle_path = "pickle\\" + str(pmid) + ".bin"
				if os.path.exists(pickle_path):
					pickled_pmids.append(pmid)
				else:
					pmids.append(pmid)

			articles = process_articles(pmids)

			for article in articles:
				pickle_path = "pickle\\" + str(article["pmid"]) + ".bin"
				f = file(pickle_path, 'wb')
				cPickle.dump(article, f, protocol=cPickle.HIGHEST_PROTOCOL)
				f.close()

			for pmid in pickled_pmids:
				pickle_path = "pickle\\" + str(pmid) + ".bin"
				f = file(pickle_path, 'rb')
				loaded_article = cPickle.load(f)
				f.close()
				articles.append(loaded_article)

			print_results (author_id, author, author_test_pattern, articles, prefix_path, pubmed_added_cutoff_date, date_published_cutoff_date)

		except Exception, e:	
			print "\n\n[--------------------------------------"
			print "Eror on pid " + str(pid) + " on author " + str(author_index)
			print e
			print "{",
			print line.replace("\t","|"),
			print "}"
			traceback.print_exc(file=sys.stdout)
			print "--------------------------------------]\n\n"

def load_domain_pmids(domain_input_file_path):
	if not domain_input_file_path:
		return []
	f = codecs.open(domain_input_file_path, "r", "utf-8")
	pmids = [line.strip() for line in f.readlines()]
	f.close()
	return pmids

def main(input_file_path, 
	output_folder, 	
	date_published_cutoff_date = "1990-01-01",
	pubmed_added_cutoff_date = "1990-01-01",
	authors_n = 100000, 
	max_processes = 4,
	domain_input_file_path = None):

	domain_pmids_list = load_domain_pmids(domain_input_file_path)
	if domain_pmids_list:
		print "domain pmids loaded: "  + str(len(domain_pmids_list))

	pubmed_added_cutoff_date = dateparser.parse(pubmed_added_cutoff_date);
	date_published_cutoff_date = dateparser.parse(date_published_cutoff_date);
	earliest_search_date = min([pubmed_added_cutoff_date, date_published_cutoff_date])
	dates = [pubmed_added_cutoff_date, date_published_cutoff_date, earliest_search_date]

	f = codecs.open(input_file_path, "r", "utf-8")
	lines = [line.strip() for line in f.readlines()]
	f.close()
	
	line_queue, output_files_prefix_queue  = Queue(), Queue()

	if authors_n <= 0 or authors_n > len(lines):
		lines_to_process = len(lines)
	else: 
		lines_to_process = (authors_n + 1) if authors_n < len(lines) else len(lines)

	for line_index in range (1, lines_to_process):
		line_queue.put((line_index, lines[line_index]))

	procs = []
	for pid in range(0, max_processes):
		proc = Process(target=multi_main, args=(pid + 1, dates, output_folder, line_queue, output_files_prefix_queue, domain_pmids_list),)
		procs.append(proc)
		proc.start()
	
	for proc in procs:
		proc.join()

	output(output_folder, output_files_prefix_queue)

if __name__ == "__main__":
	UTF8Writer = codecs.getwriter('utf8')
	sys.stdout = UTF8Writer(sys.stdout)

	main(sys.argv[1], #input_file_path
		sys.argv[2], #output_folder
		sys.argv[3], #pubmed_added_cutoff_date
		sys.argv[4]) #date_published_cutoff_date
