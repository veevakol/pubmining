# -*- coding: UTF-8 -*-
import sys
import codecs
import table_reader
import accent_remover

reload(sys)
sys.setdefaultencoding('utf8')
UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)

def process_input_file(input_file_path, output_file_path):
    output_array = []
    rows = table_reader.read_table(input_file_path, "\t")
    for row in rows:

    outfile = codecs.open(output_file_path, "wb", "utf-8")
    outfile.write("\t".join(fieldnames) + "\n")
    for rows in output_array:
        for i in range(0, len(fieldnames)):
            fld = fieldnames[i]
            outfile.write(rows[fld])
            outfile.write("\n" if i == len(fieldnames) - 1 else "\t")
    outfile.close()
    return

if __name__ == "__main__":
    process_input_file(sys.argv[1], sys.argv[2])
    print "Done!"
__author__ = 'Suhail'
