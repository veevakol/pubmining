import sys 
reload(sys) 
sys.setdefaultencoding("utf-8")
import csv
import unicodedata
import io

def remove_accents(input_str):
    nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])


with io.open("C:\\My\\395_input.txt","r",1) as f:
    read = csv.reader(f)
    for row in read:
        for element in row:
            print remove_accents(element)

print "Done"