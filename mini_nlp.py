from nltk.corpus import stopwords
import re
from collections import Counter
import math

STOP_WORDS_PATTERN = re.compile(r'\b(' + r'|'.join(stopwords.words('english')) + r')\b\s*')
WORD_PATTERN = re.compile(r'\w+')

def remove_noise_words(s):
    if s == None:
        return ""
    return STOP_WORDS_PATTERN.sub("", s).strip()

def get_cosine_on_vectors(vec1, vec2):
     intersection = set(vec1.keys()) & set(vec2.keys())
     numerator = sum([vec1[x] * vec2[x] for x in intersection])

     sum1 = sum([vec1[x]**2 for x in vec1.keys()])
     sum2 = sum([vec2[x]**2 for x in vec2.keys()])
     denominator = math.sqrt(sum1) * math.sqrt(sum2)

     if not denominator:
        return 0.0
     else:
        return float(numerator) / denominator

def text_to_vector(text):
     words = WORD_PATTERN.findall(text)
     return Counter(words)

def calc_cosine_on_text(text1, text2):
    vec1 = text_to_vector(text1)
    vec2 = text_to_vector(text2)
    return get_cosine_on_vectors(vec1, vec2)
