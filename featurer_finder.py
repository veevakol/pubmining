import re
import data_cleanser
import affiliation_extractor
import librarian
import country_detector

def is_text_key_good(key):
    if len(key.strip()) < 3:
        return False
    elif unicode(key.strip()).isnumeric():
        return False
    else:
        return True

def get_safe_key(key):
    key = re.sub("[^\w\s]", " ", key)
    key = re.sub("\s+", " ", key)
    return key.strip().lower()

def generate_co_authors_scores(precursor, feature_scores):
    feature_scores.setdefault("authors",{})
    for author in precursor["article"]["authors"]:
        if author["authorship_index"] != precursor["match"]["authorship_index"] and len(author["last_name"]) > 0:
            if len(author["first_name"]) > 0:
                feature_scores["authors"][get_safe_key((author["first_name"][0] + " " + author["last_name"]))] = 1
            else:
                feature_scores["authors"][get_safe_key((author["last_name"]))] = 1

def generate_mesh_scores(precursor, feature_scores):
    feature_scores.setdefault("mesh",{})
    for mesh in precursor["article"]["major_mesh_list"]:
        feature_scores["mesh"][get_safe_key(mesh)] = 1

def generate_journal_scores(precursor, feature_scores):
    feature_scores.setdefault("journal",{})
    feature_scores["journal"][get_safe_key(precursor["article"]["journal"])] = 1

def generate_text_scores(precursor, feature_scores):
    feature_scores.setdefault("text",{})
    text = precursor["article"]["title"]
    if text[-1] != ".":
        text += "."
    text += " " + data_cleanser.remove_extra_spaces(data_cleanser.remove_punctuation(precursor["article"]["abstract"])) if precursor["article"]["abstract"] else ""

    for phrase in data_cleanser.extract_words(text):
        if phrase and is_text_key_good(phrase):
            phrase_key = get_safe_key(phrase)
            feature_scores["text"].setdefault(phrase_key, 0)
            feature_scores["text"][phrase_key] += 1

def generate_e_mail_and_affiliation_scores(precursor, feature_scores):

    affiliation = None
    if precursor["match"]["affiliation"]:
        affiliation = precursor["match"]["affiliation"]
    elif precursor["article"]["authors"][0]["affiliation"]:
        affiliation = precursor["article"]["authors"][0]["affiliation"]

    if affiliation:
        affiliation, e_mail = affiliation_extractor.extract_affiliation_and_email(affiliation)
        affiliation = data_cleanser.remove_extra_spaces(data_cleanser.remove_punctuation(affiliation_extractor.remove_affiliation_noise_words(affiliation)))

        if affiliation:
            feature_scores["affiliation"] = affiliation
            country = country_detector.detect_country(affiliation)
            if country:
                feature_scores["country"] = country

        if e_mail:
            feature_scores["e_mail"] = e_mail

def get_article_feature_scores (precursor):
    scores = {}
    generate_co_authors_scores(precursor, scores)
    generate_mesh_scores(precursor, scores)
    generate_journal_scores(precursor, scores)
    generate_text_scores(precursor, scores)
    generate_e_mail_and_affiliation_scores(precursor, scores)
    return scores

def tabulate_scores_freqs (article_text_features, text_scores, text_freq):
    for feature, score in article_text_features.items():
        text_scores.setdefault(feature, 0)
        text_freq.setdefault(feature, 0)
        text_scores[feature] += score
        text_freq[feature] += 1

def update_key_features (feature_freq, feature_scores, n_articles, start_range, end_range):
    key_feature_list = []
    for feature, freq in feature_freq.items():
        frac = float(freq) / n_articles
        if (frac > start_range and frac < end_range and len(feature) > 2 and freq > 1):
            key_feature_list.append(feature)
        else:
            del feature_scores[feature]
            del feature_freq[feature]

def get_features(job):
    article_features = {}
    text_scores, text_freq = {}, {}
    author_scores, author_freq = {}, {}

    precursor_list = librarian.get_precursors(job["request"]["author_id"])

    for precursor in precursor_list:
        #article_features[precursor["pmid"]] = get_article_feature_scores (precursor)
        article_features[precursor["pmid"]] = get_article_feature_scores (precursor)
        tabulate_scores_freqs(article_features[precursor["pmid"]]["text"], text_scores, text_freq)
        tabulate_scores_freqs(article_features[precursor["pmid"]]["authors"], author_scores, author_freq)
        article_features[precursor["pmid"]]["found_name"] = precursor["match"]["first_name"] + " " + \
                                                  precursor["match"]["last_name"]

    update_key_features(text_freq, text_scores, len(precursor_list), 0.1, 0.75)
    update_key_features(author_freq, author_scores, len(precursor_list), 0, 1)

    my_matrix = {
        "_id": job["request"]["author_id"],
        "author": job["request"]["first_name"] + " " + job["request"]["last_name"],
        "article_features": article_features
    }

    return my_matrix, article_features

def create_matrix(job):
    my_matrix, matrix = get_features(job)
    librarian.save_matrix(my_matrix)

if __name__ == "__main__":
    pass
    #create_feature_set(r"susan_banerjee.txt", "matrix_susan_banerjee.txt", "kcluster_susan_banerjee.txt")
