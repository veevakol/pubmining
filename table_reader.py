from collections import OrderedDict

#def read_table(file_path, delimiter, encoding="cp1252"):
def read_table(file_path, delimiter, encoding="utf-8"):
    import io
    print file_path
    f = io.open(file_path, mode="r", buffering=1, encoding=encoding)
    i, header, rows = 0, None, []
    for line in f:
        if i == 0:
            header = [col.strip() for col in line.split(delimiter)]
        else:
            cells, row = line.split(delimiter), OrderedDict()
            for n in range(0, len(header)):
                row[header[n]] = cells[n].strip()
            rows.append(row)
        i += 1
    f.close()
    return rows
