import librarian
import codecs
import HTMLParser
#from datetime import datetime

_authorship  = {
    -1: "Last Author",
    0: "First Author",
    1: "Coauthor"
}

if __name__ == "__main__":
    o = codecs.open(r"./article_exporter.txt", "w", "utf-8")
    header = []
    header.append("author_id")
    header.append("author_name")
    header.append("pmid")
    header.append("group_id")
    header.append("found_name")
    header.append("found_affiliation")
    header.append("affiliation_source")
    header.append("display_date")
    header.append("year")
    header.append("authorship_index")
    header.append("authorship_position")
    header.append("title")
    header.append("journal")
    header.append("major_mesh")
    header.append("mamba_article_type")
    header.append("coauthors")
    header.append("publication_status")
    o.write("\t".join(header) + "\n")
    dataset = librarian.get_articles_for_all_authors()

    h = HTMLParser.HTMLParser()

    clusters = {}

    for data in dataset:
        row = []
        author_id = data["author_id"]
        row.append(author_id)
        if author_id not in clusters:
            try:             
                author_clusters = librarian.get_clusters_for_author(author_id)
                clusters[author_id] = author_clusters["cluster"]["articles"]
            except Exception, e:
                print "Error gettig author_clusters for author id " + author_id 
                print str(e)
                continue 

        row.append(data["author_first_name"] + " " + data["author_last_name"])
        row.append(data["pmid"])
        if data["pmid"] not in clusters[author_id]:
            row.append("?")
        else:
            row.append(str(clusters[author_id][data["pmid"]]))
        row.append(data["match"]["first_name"] + " " + data["match"]["last_name"])
        if data["match"]["affiliation"]:
            row.append(h.unescape(data["match"]["affiliation"]))
            row.append("Author")
        elif data["article"]["authors"][0]["affiliation"]:
            row.append(h.unescape(data["article"]["authors"][0]["affiliation"]))
            row.append("Article")
        else:
            row.append("(na)")
            row.append("(na)")
        try:
            row.append(data["article"]["pub_display_date"].strftime("%Y-%m-%d"))
        except AttributeError:
            row.append("Error")
            data["pmid"] + " has date errorerror."

        row.append(str(data["article"]["year"]))
        row.append(str(data["match"]["authorship_index"]))
        row.append(_authorship[1 if data["match"]["authorship_index"] > 0 else data["match"]["authorship_index"]])
        row.append(h.unescape(data["article"]["title"]))
        row.append(h.unescape(data["article"]["journal"]))
        row.append("; ".join(data["article"]["major_mesh_list"]))
        row.append(data["article"]["mamba_type"])
        row.append("; ".join(author["first_name"] + " " + author["last_name"]
                   for author in data["article"]["authors"]))
        row.append(data["article"]["publication_status"])
        o.write("\t".join(row) + "\n")
    o.close()
    print "Done"